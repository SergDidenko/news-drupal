jQuery(document).ready(
    function($) {
        $('.date-picker').datepicker(
            {
                changeMonth: true,
                changeYear: true,
                showButtonPanel: true,
                dateFormat: 'MM yy',
                constrainInput: true,
                showOn: 'button',
                buttonText: 'Select...'
            }
        ).focus(
            function() {
                var thisCalendar = $(this);
                $('.ui-datepicker-calendar').detach();
                $('.ui-datepicker-close').click(
                    function() {
                        var month = $("#ui-datepicker-div .ui-datepicker-month :selected").val();
                        var year = $("#ui-datepicker-div .ui-datepicker-year :selected").val();
                        thisCalendar.datepicker('setDate', new Date(year, month, 1));
                        var settings = {
                            url: 'news/ajax'
                        }
                        var ajax = new Drupal.ajax(false, false, settings);
                        ajax.options.data['month'] = +month + 1;
                        ajax.options.data['year'] = year;
                        ajax.eventResponse(ajax, {});
                    }
                );
            }
        );
    }
);
