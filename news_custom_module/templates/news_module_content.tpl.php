<?php $items = $variables['items']; ?>

<?php foreach ($items as $nid=>$news): ?>
    <div class="<?php print 'last-news'; ?>">
        <h4><span class="last-news_field">Title:</span> <?php print $news->title; ?></h4>
        <h4><span class="last-news_field">Subtitle: </span><?php print $news->subtitle['und'][0]['value']; ?></h4>
        <h4><span class="last-news_field">Description: </span><?php print $news->description['und'][0]['value']; ?></h4>
        <span class='last-news_link'><?php print l('Read more', drupal_get_path_alias('node/'.$nid)); ?></span>
    </div>
<?php endforeach; ?>
